// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312_Escape.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GAM312_Escape, "GAM312_Escape" );

DEFINE_LOG_CATEGORY(EnemyAI);

DEFINE_LOG_CATEGORY(PlayerCon);

DEFINE_LOG_CATEGORY(PlayerChar);

DEFINE_LOG_CATEGORY(PowerUp);

DEFINE_LOG_CATEGORY(EscapeUI);

DEFINE_LOG_CATEGORY(EscapeGameMode);