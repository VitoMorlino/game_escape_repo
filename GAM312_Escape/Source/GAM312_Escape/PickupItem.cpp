// Designed and Created by: Vito Morlino

#include "PickupItem.h"


// Sets default values
APickupItem::APickupItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create Box Trigger Root Component
	TriggerSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerSphere"));
	TriggerSphere->bGenerateOverlapEvents = true;
	TriggerSphere->OnComponentBeginOverlap.AddDynamic(this, &APickupItem::TriggerEntered);
	TriggerSphere->OnComponentEndOverlap.AddDynamic(this, &APickupItem::TriggerExited);
	RootComponent = TriggerSphere;

	// Create Static Mesh component
	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemMesh"));
	ItemMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APickupItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (MyPlayer != NULL) //Check if MyPlayer was initialized
	{
		//If Action key is pressed and player is in range
		if (MyPlayer->bIsPickingUp && bIsInRange)
		{
			Pickup();
		}
	}

}

// Destroy self and add to player's inventory
void APickupItem::Pickup()
{
	Cast<AEscapePlayerController>(MyPlayer->GetController())->IncrementCollectedNumOfItems();
	UE_LOG(LogTemp, Log, TEXT("You picked up %s!"), *ItemName);
	Destroy();

	if (PickupSound && MyPlayer)
		UGameplayStatics::PlaySoundAtLocation(this, PickupSound, MyPlayer->GetActorLocation());
}

//When player enters the trigger box
void APickupItem::TriggerEntered(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	APlayerCharacter* IsPlayer = Cast<APlayerCharacter>(OtherActor);
	if (IsPlayer)
	{
		MyPlayer = IsPlayer;

		// Used by custom PostProcess to render outlines
		ItemMesh->SetRenderCustomDepth(true);

		bIsInRange = true;
	}
}

//When player exits the trigger box
void APickupItem::TriggerExited(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	APlayerCharacter* WasPlayer = Cast<APlayerCharacter>(OtherActor);
	if (WasPlayer)
	{
		// Used by custom PostProcess to render outlines
		ItemMesh->SetRenderCustomDepth(false);

		bIsInRange = false;
	}
}

