// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "AIPatrolController.h"
#include "BTSelectPatrolPoint.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API UBTSelectPatrolPoint : public UBTTaskNode
{
	GENERATED_BODY()
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
	
};
