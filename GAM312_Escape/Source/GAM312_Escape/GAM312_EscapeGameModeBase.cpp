// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312_EscapeGameModeBase.h"
#include "EscapePlayerController.h"

//

//AGAM312_EscapeGameModeBase::AGAM312_EscapeGameModeBase()
//{
//	MainMusicAudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("MainMusicAudioComp"));
//	MainMusicAudioComp->SetupAttachment(RootComponent);
//	MainMusicAudioComp->SetAutoActivate(true);
//
//	AIChaseMusicAudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AIChaseMusicAudioComp"));
//	AIChaseMusicAudioComp->SetupAttachment(RootComponent);
//	AIChaseMusicAudioComp->SetAutoActivate(false);
//}

void AGAM312_EscapeGameModeBase::StartPlay()
{
	//Create Empty DefaultSaveGame
	DefaultSaveGame = Cast<UEscapeSaveGame>(UGameplayStatics::CreateSaveGameObject(UEscapeSaveGame::StaticClass()));

	//If SaveGame does NOT exist
	if (!UGameplayStatics::DoesSaveGameExist(DefaultSaveGame->SaveSlotName, DefaultSaveGame->UserIndex))
	{
		UE_LOG(EscapeGameMode, Warning, TEXT("SaveGame does not exist --- Creating new SaveGame -GAM312_EscapeGameModeBase.cpp"));

		//Set IsFirstTimePlaying to true
		IsFirstTimePlaying = true;

		//Save the New DefaultSaveGame to the default SaveSlotName and UserIndex
		UGameplayStatics::SaveGameToSlot(DefaultSaveGame, DefaultSaveGame->SaveSlotName, DefaultSaveGame->UserIndex);
	}
	else
	{
		UE_LOG(EscapeGameMode, Warning, TEXT("SaveGame found --- Loading SaveGame -GAM312_EscapeGameModeBase.cpp"));

		//Set IsFirstTimePlaying to false
		IsFirstTimePlaying = false;
	}

	//Load the found or newly created SaveGame
	LoadGame();

	//Start BeginPlay() calls on actors
	Super::StartPlay();

	//Create MainMusic and Start playing it
	if (MainMusic)
	{
		MainMusicAudioComp = UGameplayStatics::CreateSound2D(this, MainMusic, 1.f, 1.f, 0.f, (USoundConcurrency *)nullptr, true, false);
		if (MainMusicAudioComp)
		{
			MainMusicAudioComp->bAutoActivate = true;
			MainMusicAudioComp->bIsUISound = false;
			MainMusicAudioComp->Play();
		}
		else
			UE_LOG(EscapeGameMode, Error, TEXT("MainMusicAudioComp not found -GAM312_EscapeGameModeBase.cpp"));
	}
	else
		UE_LOG(EscapeGameMode, Warning, TEXT("MainMusic not found -GAM312_EscapeGameModeBase.cpp"));

	//Create AIChaseMusic, but don't play it yet
	if (AIChaseMusic)
	{
		AIChaseMusicAudioComp = UGameplayStatics::CreateSound2D(this, AIChaseMusic, 1.f, 1.f, 0.f, (USoundConcurrency *)nullptr, false, false);

		if (AIChaseMusicAudioComp)
		{
			AIChaseMusicAudioComp->bAutoActivate = false;
			AIChaseMusicAudioComp->bIsUISound = false;
		}
		else
			UE_LOG(EscapeGameMode, Error, TEXT("AIChaseMusicAudioComp not found -GAM312_EscapeGameModeBase.cpp"));
	}
	else
		UE_LOG(EscapeGameMode, Warning, TEXT("AIChaseMusic not found -GAM312_EscapeGameModeBase.cpp"));

	//Create MenuMusic, but don't play it yet
	if (MenuMusic)
	{
		MenuMusicAudioComp = UGameplayStatics::CreateSound2D(this, MenuMusic, 1.f, 1.f, 0.f, (USoundConcurrency *)nullptr, false, false);

		if (MenuMusicAudioComp)
		{
			MenuMusicAudioComp->bAutoActivate = true;
			MenuMusicAudioComp->bIsUISound = true;
			//Start Playing...
			MenuMusicAudioComp->Play();
			//...but immediately pause it
			MenuMusicAudioComp->SetPaused(true);
		}
		else
			UE_LOG(EscapeGameMode, Error, TEXT("MenuMusicAudioComp not found -GAM312_EscapeGameModeBase.cpp"));
	}
	else
		UE_LOG(EscapeGameMode, Warning, TEXT("MenuMusic not found -GAM312_EscapeGameModeBase.cpp"));
}

void AGAM312_EscapeGameModeBase::ResetLevel()
{
	Super::ResetLevel();

	UE_LOG(EscapeGameMode, Warning, TEXT("ResetLevel Called -GAM312_EscapeGameModeBase.cpp"));
}

void AGAM312_EscapeGameModeBase::Logout(AController * Exiting)
{
	Super::Logout(Exiting);

	if (MainMusicAudioComp)
		MainMusicAudioComp->Stop();
	if (AIChaseMusicAudioComp)
		AIChaseMusicAudioComp->Stop();
}

void AGAM312_EscapeGameModeBase::SaveGame(float BestTime, float BestTimeAllItems, FStructHasSeenHelp HelpStruct)
{
	UE_LOG(EscapeGameMode, Warning, TEXT("SaveGame(%f, %f, HelpStruct) called -GAM312_EscapeGameModeBase.cpp"), BestTime, BestTimeAllItems);

	//Create Empty NewSaveGame
	UEscapeSaveGame* NewSaveGame = Cast<UEscapeSaveGame>(UGameplayStatics::CreateSaveGameObject(UEscapeSaveGame::StaticClass()));

	//Set variables in NewSaveGame to those passed into function
	NewSaveGame->BestTime = BestTime;
	NewSaveGame->BestTimeAllItems = BestTimeAllItems;
	NewSaveGame->SavedHelpStruct = HelpStruct;

	//Save the NewSaveGame to the default SaveSlotName and UserIndex
	UGameplayStatics::SaveGameToSlot(NewSaveGame, NewSaveGame->SaveSlotName, NewSaveGame->UserIndex);

	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, TEXT("Game Saved"));

	UE_LOG(EscapeGameMode, Warning, TEXT("Saved Game to SlotName: \"%s\", UserIndex: \"%d\" -GAM312_EscapeGameModeBase.cpp"), *NewSaveGame->SaveSlotName, NewSaveGame->UserIndex);
}

void AGAM312_EscapeGameModeBase::LoadGame()
{
	if (DefaultSaveGame) //Null check
	{
		//If SaveGame exists
		if (UGameplayStatics::DoesSaveGameExist(DefaultSaveGame->SaveSlotName, DefaultSaveGame->UserIndex))
		{
			//Load the SaveGame saved at the default SaveSlotName and UserIndex
			LoadedInstance = Cast<UEscapeSaveGame>(UGameplayStatics::LoadGameFromSlot(DefaultSaveGame->SaveSlotName, DefaultSaveGame->UserIndex));
		}
		else
			UE_LOG(EscapeGameMode, Error, TEXT("Tried to LoadGame() but SaveGame does not exist! -GAM312_EscapeGameModeBase.cpp"));
	}
	else
		UE_LOG(EscapeGameMode, Error, TEXT("DefaultSaveGame not found LoadGame-GAM312_EscapeGameModeBase.cpp"));
}

void AGAM312_EscapeGameModeBase::StartChaseMusic()
{
	//If The Chase is not already on...
	if (!bTheChaseIsOn)
	{
		//THE CHASE IS ON!
		bTheChaseIsOn = true;

		//Pause MainMusic
		if (MainMusic && MainMusicAudioComp)
		{
			MainMusicAudioComp->SetPaused(true);
		}

		//Play AIChaseMusic
		if (AIChaseMusic && AIChaseMusicAudioComp)
		{
			AIChaseMusicAudioComp->Play();
		}
	}
}

void AGAM312_EscapeGameModeBase::EndChaseMusic()
{
	//If The Chase is not already off...
	if (bTheChaseIsOn)
	{
		//The Chase is off
		bTheChaseIsOn = false;

		//Stop AIChaseMusic
		if (AIChaseMusic && AIChaseMusicAudioComp)
		{
			AIChaseMusicAudioComp->Stop();
		}

		//Unpause MainMusic
		if (MainMusic && MainMusicAudioComp)
		{
			MainMusicAudioComp->SetPaused(false);
		}
	}
}
