// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GAM312_Escape.h"
#include "GAM312_EscapeGameModeBase.h"
#include "AIController.h"
#include "AIPatrol.h"
#include "AIPatrolPoint.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIPatrolController.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API AAIPatrolController : public AAIController
{
	GENERATED_BODY()


	//Reference to AICharacter possessed by this controller
	AAIPatrol* AICharacter;

	//Behavior Tree Component
	UBehaviorTreeComponent* BehaviorComp;

	//Blackboard Component
	UBlackboardComponent* BlackboardComp;

	//Blackboard Keys
	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName LocationToGoKey;
	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName PlayerKey;
	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName CanSeePlayerKey;
	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName IsPatrolKey;
	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName ShouldReturnToPostKey;

	//Array of PatrolPoints to path to
	TArray<AActor*> PatrolPoints;

	//References to Starting Transform/Position
	FVector StartingLocation;
	FRotator StartingRotation;

	//Handle to manage timer
	FTimerHandle CanSeePlayerTimerHandle;

	/** When AI loses sight of the player, this is
	*	how long they wait before giving up */
	float CanSeePlayerCooldown = 3.f; //Time in seconds

	//Override possession of pawn
	virtual void Possess(APawn* Pawn) override;

public:
	//Constructor
	AAIPatrolController(const FObjectInitializer& ObjectInitializer);

	//Holds the index of the current patrol point
	int32 CurrentPatrolPoint = 0;

	// Store Pawn caught as PlayerKey in BB
	void SetPlayerCaught(APlayerCharacter* Player);

	//Called when a player is found
	void FoundPlayer();

	//Called when the player is lost
	void LostPlayer();

	//Inline Gets
	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
	FORCEINLINE TArray<AActor*> GetPatrolPoints() const { return PatrolPoints; }
	FORCEINLINE AAIPatrol* GetAICharacter() const { return AICharacter; }
	FORCEINLINE FVector GetStartingLocation() const { return StartingLocation; }
	FORCEINLINE FRotator GetStartingRotation() const { return StartingRotation; }
};
