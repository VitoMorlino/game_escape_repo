// Designed and Created by: Vito Morlino

#include "PowerUp.h"


// Sets default values
APowerUp::APowerUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create Box Trigger Root Component
	TriggerSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerSphere"));
	TriggerSphere->bGenerateOverlapEvents = true;
	TriggerSphere->OnComponentBeginOverlap.AddDynamic(this, &APowerUp::TriggerEntered);
	RootComponent = TriggerSphere;

	PowerUpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PowerUpMesh"));
	PowerUpMesh->SetupAttachment(RootComponent);
	PowerUpMesh->SetCollisionProfileName("OverlapAllDynamic");
	PowerUpMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);

	//Create Particle System (Empty until particles are set in derived class)
	PowerUpParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("PowerUpParticles"));
	PowerUpParticles->SetupAttachment(RootComponent);
	PowerUpParticles->bAutoActivate = true;

	MovementSoundAudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComp"));
	MovementSoundAudioComp->SetupAttachment(RootComponent);
	MovementSoundAudioComp->SetAutoActivate(true);


}

// Called when the game starts or when spawned
void APowerUp::BeginPlay()
{
	Super::BeginPlay();

	InitialLoc = GetActorLocation();
	TargetLoc = InitialLoc;
	TargetLoc.Z += MaxFloatHeight;
	MidPoint = (InitialLoc.Z + TargetLoc.Z) / 2;

	if (MovementSound && MovementSoundAudioComp)
	{
		MovementSoundAudioComp->SetSound(MovementSound);
		MovementSoundAudioComp->Play();
	}
	
}

// Called every frame
void APowerUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

/*********************************************************************/
/******** Make actor 'float' up and down *****************************/
/*        ******************************                             */
/*                                                                   */
	
	FVector CurrentLoc = GetActorLocation();

	float DistanceFromInitial = CurrentLoc.Z - InitialLoc.Z;
	float DistanceFromTarget = TargetLoc.Z - CurrentLoc.Z;
	float PercentDistFromInitial = DistanceFromInitial / MaxFloatHeight;
	float PercentDistFromTarget = DistanceFromTarget / MaxFloatHeight;
	PercentDistFromInitial = FMath::Clamp<float>(PercentDistFromInitial, 0.01f, MaxFloatSpeed);
	PercentDistFromTarget = FMath::Clamp<float>(PercentDistFromTarget, 0.01f, MaxFloatSpeed);

	DeltaTime *= FloatAccelMultiplier;

	//If we're going up
	if (bShouldFloatUp)
	{
		//If we're below the MidPoint
		if (CurrentLoc.Z < MidPoint)
		{
			//Speed up away from InitialLoc
			Alpha += DeltaTime * PercentDistFromInitial;

			if (MovementSound)
				MovementSound->VolumeMultiplier = PercentDistFromInitial * 2;

		}
		//else if we're above the MidPoint
		else
		{
			//Slow down to TargetLoc.Z
			Alpha += DeltaTime * PercentDistFromTarget;

			if (MovementSound)
				MovementSound->VolumeMultiplier = PercentDistFromTarget * 2;

			//If PercentDistFromTarget ~= Zero
			if (FMath::IsNearlyZero(PercentDistFromTarget, 0.01f))
				bShouldFloatUp = false;
		}
	}
	//else if we're going down
	else
	{
		//If we're above the MidPoint
		if (CurrentLoc.Z > MidPoint)
		{
			//Speed up away from TargetLoc
			Alpha -= DeltaTime * PercentDistFromTarget;

			if (MovementSound)
				MovementSound->VolumeMultiplier = PercentDistFromTarget * 2;

		}
		//else if we're below the MidPoint
		else
		{
			//Slow down to InitialLoc.Z
			Alpha -= DeltaTime * PercentDistFromInitial;

			if (MovementSound)
				MovementSound->VolumeMultiplier = PercentDistFromInitial * 2;

			//If PercentDistFromInitial ~= Zero
			if (FMath::IsNearlyZero(PercentDistFromInitial, 0.01f))
				bShouldFloatUp = true;
		}
	}

	Alpha = FMath::Clamp<float>(Alpha, 0.0f, 1.0f);
	
	SetActorLocation(FMath::Lerp(InitialLoc, TargetLoc, Alpha));
/*                                                                   */
/*        ******************************                             */
/*********************************************************************/

}

void APowerUp::TriggerEntered(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	MyPlayer = Cast<APlayerCharacter>(OtherActor);

	if (MyPlayer)
	{
		if (!MyPlayer->bIsPoweredUp)
		{
			Pickup();
		}
		else
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("* You can only have one PowerUp active *"));
	}
}

void APowerUp::Pickup()
{
	UE_LOG(PowerUp, Log, TEXT("Picked up %s! -PowerUp.cpp"), *PowerUpName);
	
	//ActivatePowerUp
	ActivatePowerUp(); //This calls ActivatePowerUp() in the derived class

	MovementSoundAudioComp->Stop();
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);

	if (PickupSound && MyPlayer)
		UGameplayStatics::PlaySoundAtLocation(this, PickupSound, MyPlayer->GetActorLocation());
}

//Called with Super::ActivatePowerUp() from derived class
void APowerUp::ActivatePowerUp()
{
	//Start timer (Calls DeactivatePowerUp() in the derived class when timer ends)
	GetWorldTimerManager().SetTimer(PowerUpTimer, this, &APowerUp::DeactivatePowerUp, PowerUpDuration, false);

	if (MyPlayer)
	{
		//Set bIsPoweredUp to true
		MyPlayer->bIsPoweredUp = true;

		//Set player character's mesh material to PoweredUpMaterial
		MyPlayer->GetMesh()->SetMaterial(0, MyPlayer->PoweredUpMaterial);
	}
	else
		UE_LOG(PowerUp, Error, TEXT("MyPlayer not found ActivatePowerup-PowerUp.cpp"));
}

//Called with Super::DeactivatePowerUp() from derived class
void APowerUp::DeactivatePowerUp()
{
	if (MyPlayer)
	{
		//Set bIsPoweredUp to false
		MyPlayer->bIsPoweredUp = false;

		//Set player character's mesh material to UnpoweredMaterial
		MyPlayer->GetMesh()->SetMaterial(0, MyPlayer->UnpoweredMaterial);
		MyPlayer->GetMesh()->SetMaterial(1, MyPlayer->PoweredUpMaterial);
	}
	else
		UE_LOG(PowerUp, Error, TEXT("MyPlayer not found DeactivatePowerUp-PowerUp.cpp"));

	//Clear timer
	GetWorldTimerManager().ClearTimer(PowerUpTimer);

	//Destroy this actor
	Destroy();
}