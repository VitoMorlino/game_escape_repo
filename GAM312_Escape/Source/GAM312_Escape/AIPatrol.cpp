// Designed and Created by: Vito Morlino

#include "AIPatrol.h"
#include "AIPatrolController.h"


// Sets default values
AAIPatrol::AAIPatrol()
{
	//Initialize Pawn Sensors
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
	PawnSensingComp->SetPeripheralVisionAngle(50.f);
	PawnSensingComp->SightRadius = 1100.f;
	PawnSensingComp->HearingThreshold = 1000.f;

	//Set walk speed to a value less than the player's
	GetCharacterMovement()->MaxWalkSpeed = DefaultWalkSpeed;

	//Turn smoothly instead of snapping to target rotation
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	bUseControllerRotationYaw = false;

	// Create reference to pulsing material for character mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MatPulseAsset(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile_Pulse.M_Tech_Hex_Tile_Pulse"));
	if (MatPulseAsset.Succeeded())
		MatPulse = MatPulseAsset.Object;

	// Create reference to non-pulsing material for character mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MatNoPulseAsset(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile.M_Tech_Hex_Tile"));
	if (MatNoPulseAsset.Succeeded())
		MatNoPulse = MatNoPulseAsset.Object;
}

// Called when the game starts or when spawned
void AAIPatrol::BeginPlay()
{
	Super::BeginPlay();

	//Bind delegate
	if (PawnSensingComp)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &AAIPatrol::OnPlayerCaught);
	}
	else
		UE_LOG(EnemyAI, Error, TEXT("PawnSensingComp not found -AIPatrol.cpp"));
}

void AAIPatrol::OnPlayerCaught(APawn * Pawn)
{
	// Try to cast Pawn to PlayerCharacter
	APlayerCharacter* Player = Cast<APlayerCharacter>(Pawn);

	// If cast was successful
	if (Player)
	{
		// Get reference to AIController
		AAIPatrolController* AIController = Cast<AAIPatrolController>(GetController());

		//Call SetPlayerCaught on the AIController Player sensed
		if (AIController)
		{
			AIController->SetPlayerCaught(Player);
			UE_LOG(EnemyAI, Log, TEXT("AI Sensed Player! -AIPatrol.cpp"));
		}
		else
			UE_LOG(EnemyAI, Error, TEXT("AIController not found -AIPatrol.cpp"));
	}
	else
		UE_LOG(EnemyAI, Error, TEXT("Player not found -AIPatrol.cpp"));
	
}