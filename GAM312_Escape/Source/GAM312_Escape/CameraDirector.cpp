// Designed and Created by: Vito Morlino

#include "CameraDirector.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACameraDirector::ACameraDirector()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}


// Called when the game starts or when spawned
void ACameraDirector::BeginPlay()
{
	Super::BeginPlay();

	//Find the actor that handles control for the local player.
	OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);

}

// Called every frame
void ACameraDirector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Start Overhead View
void ACameraDirector::StartOverheadView()
{

	if (OurPlayerController)
	{
		//Set Follow camera to player's viewtarget
		PerspectiveCam = OurPlayerController->GetViewTarget();

		//Blend view to new camera
		OurPlayerController->SetViewTargetWithBlend(OverheadCam, SmoothBlendTime);

		// screen message
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Started Overhead View"));
	}
}

//Stop Overhead View
void ACameraDirector::StopOverheadView()
{
	if (OurPlayerController)
	{
		//Blend view to new camera
		OurPlayerController->SetViewTargetWithBlend(PerspectiveCam, SmoothBlendTime);

		// screen message
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Stopped Overhead View"));
	}
}