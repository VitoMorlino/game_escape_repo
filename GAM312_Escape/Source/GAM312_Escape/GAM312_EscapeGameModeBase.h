// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GAM312_Escape.h"
#include "GameFramework/GameModeBase.h"
#include "GAM312_EscapeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API AGAM312_EscapeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	/** Transitions to calls BeginPlay on actors. */
	UFUNCTION(BlueprintCallable, Category = Game)
	virtual void StartPlay() override;

	/**
	* Overridable function called when resetting level. This is used to reset the game state while staying in the same map
	* Default implementation calls Reset() on all actors except GameMode and Controllers
	*/
	UFUNCTION(BlueprintCallable, Category = Game)
	virtual void ResetLevel() override;

	/* Called when a Controller with a PlayerState leaves the game or is destroyed */
	virtual void Logout(AController* Exiting) override;

	bool IsFirstTimePlaying;

public:
	//AGAM312_EscapeGameModeBase();

	void SaveGame(float BestTime, float BestTimeAllItems, FStructHasSeenHelp HelpStruct);
	void LoadGame();
	UEscapeSaveGame* DefaultSaveGame;
	UEscapeSaveGame* LoadedInstance;

	FORCEINLINE bool GetIsFirstTimePlaying() { return IsFirstTimePlaying; }
	FORCEINLINE class UEscapeSaveGame* GetLoadedInstance() { return LoadedInstance; }

	/*********************************************************************/
	/****** GAME MUSIC ***************************************************/
	/*                                                                   */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameMusic")
	UAudioComponent* MainMusicAudioComp;
	UPROPERTY(EditDefaultsOnly, Category = "GameMusic")
	USoundCue* MainMusic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameMusic")
	UAudioComponent* AIChaseMusicAudioComp;
	UPROPERTY(EditDefaultsOnly, Category = "GameMusic")
	USoundCue* AIChaseMusic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameMusic")
	UAudioComponent* MenuMusicAudioComp;
	UPROPERTY(EditDefaultsOnly, Category = "GameMusic")
	USoundCue* MenuMusic;

	bool bTheChaseIsOn = false;

	void StartChaseMusic();
	void EndChaseMusic();
	/*                                                                   */
	/*********************************************************************/
};
