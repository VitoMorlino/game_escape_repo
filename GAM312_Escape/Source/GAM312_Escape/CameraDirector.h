// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "CameraDirector.generated.h"

UCLASS()
class GAM312_ESCAPE_API ACameraDirector : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACameraDirector();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	const float SmoothBlendTime = 0.5f; //How long the blend takes

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	APlayerController* OurPlayerController;

	UPROPERTY(EditAnywhere)
		AActor* OverheadCam;

	UPROPERTY(EditAnywhere)
		AActor* PerspectiveCam;

	void StartOverheadView();
	void StopOverheadView();
};
