// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GAM312_Escape.h"
#include "GameFramework/Actor.h"
#include "PlayerCharacter.h"
#include "PowerUp.generated.h"

UCLASS()
class GAM312_ESCAPE_API APowerUp : public AActor
{
	GENERATED_BODY()

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* PickupSound;

public:	
	// Sets default values for this actor's properties
	APowerUp();

protected:
	//Name of this PowerUp
	UPROPERTY(EditAnywhere)
	FString PowerUpName = FString(TEXT("*** SET THIS POWERUP'S NAME IN IT'S OWN CLASS ***"));

	UPROPERTY(EditAnywhere)
	UParticleSystemComponent *PowerUpParticles;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* MovementSound;

	UPROPERTY(EditAnywhere, Category = "Sound")
	UAudioComponent* MovementSoundAudioComp;

	FTimerHandle PowerUpTimer;

	//Override these in the derived PowerUp class
	virtual void ActivatePowerUp();
	virtual void DeactivatePowerUp();

private:
	FVector InitialLoc;
	FVector TargetLoc;
	float MidPoint;
	float MaxFloatHeight = 50.f;
	float MaxFloatSpeed = 0.2f; // (Between 0 and 1) // 0 = No Speed // 1 = Full Speed //
	float FloatAccelMultiplier = 5.f;
	float Alpha = 0;
	bool bShouldFloatUp;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Reference to PlayerCharacter
	APlayerCharacter* MyPlayer;

	UPROPERTY(EditAnywhere)
	UShapeComponent* TriggerSphere;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PowerUpMesh;

	UFUNCTION()
	void TriggerEntered(class UPrimitiveComponent * HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void Pickup();

	//PowerUpDuration Default is 5.0 seconds (Can override in derived class)
	UPROPERTY(EditAnywhere)
	float PowerUpDuration = 5.f;
};
