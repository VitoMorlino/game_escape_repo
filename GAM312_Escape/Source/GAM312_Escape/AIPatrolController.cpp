// Designed and Created by: Vito Morlino

#include "AIPatrolController.h"
#include "CustomPathFollowingComponent.h"

AAIPatrolController::AAIPatrolController(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer.SetDefaultSubobjectClass<UCustomPathFollowingComponent>(TEXT("PathFollowingComponent")))
{
	// Initialize blackboard and behavior tree
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));

	// Initialize blackboard keys
	PlayerKey = "Target";
	LocationToGoKey = "LocationToGo";
	CanSeePlayerKey = "CanSeePlayer";
	IsPatrolKey = "IsPatrol";
	ShouldReturnToPostKey = "ShouldReturnToPost";

}

void AAIPatrolController::SetPlayerCaught(APlayerCharacter* Player)
{
	//If the player has a Ghost powerup, call LostPlayer()
	if (Player->bIsGhost)
		AAIPatrolController::LostPlayer();
	else
	{
		//Call FoundPlayer()
		AAIPatrolController::FoundPlayer();

		if (BlackboardComp) //null check
		{
			//Set the seen player as the Target object in the blackboard
			BlackboardComp->SetValueAsObject(PlayerKey, Player);
			UE_LOG(EnemyAI, Warning, TEXT("Player Detected!"));
		}
		else
			UE_LOG(EnemyAI, Error, TEXT("BlackboardComp not found -AIPatrolController.cpp"));
	}

}

void AAIPatrolController::FoundPlayer()
{
	AGAM312_EscapeGameModeBase* Game = Cast<AGAM312_EscapeGameModeBase>(GetWorld()->GetAuthGameMode());
	if (Game)
		Game->StartChaseMusic();

	// Set character mesh material to MatPulse
	AICharacter->GetMesh()->SetMaterial(0, AICharacter->GetMatPulse());

	// Increase character movement speed
	AICharacter->GetCharacterMovement()->MaxWalkSpeed = AICharacter->GetDefaultWalkSpeed() + 100.f;

	if (BlackboardComp)
	{
		//Set CanSeePlayer to true
		BlackboardComp->SetValueAsBool(CanSeePlayerKey, true);

		//Start Timer
		GetWorldTimerManager().SetTimer(CanSeePlayerTimerHandle, this, &AAIPatrolController::LostPlayer, CanSeePlayerCooldown, false);
	}
	else
		UE_LOG(EnemyAI, Error, TEXT("BlackboardComp not found -AIPatrolController.cpp"));

}

void AAIPatrolController::LostPlayer()
{
	AGAM312_EscapeGameModeBase* Game = Cast<AGAM312_EscapeGameModeBase>(GetWorld()->GetAuthGameMode());
	if (Game)
		Game->EndChaseMusic();

	//Clear the CanSeePlayerTimerHandle in case this function was called from elsewhere
	GetWorldTimerManager().ClearTimer(CanSeePlayerTimerHandle);

	if (BlackboardComp)
	{
		//Set CanSeePlayer to false
		BlackboardComp->SetValueAsBool(CanSeePlayerKey, false);

		if (AICharacter)
		{
			//if AICharacter->bIsPatrol == false, Set ShouldReturnToPost to true
			if (!AICharacter->bIsPatrol)
				BlackboardComp->SetValueAsBool(ShouldReturnToPostKey, true);

			//Set AI character's mesh to MatNoPulse
			AICharacter->GetMesh()->SetMaterial(0, AICharacter->GetMatNoPulse());

			//Set AI character's movement speed back to default
			AICharacter->GetCharacterMovement()->MaxWalkSpeed = AICharacter->GetDefaultWalkSpeed();
		}
		else
			UE_LOG(EnemyAI, Error, TEXT("AICharacter not found -AIPatrolController.cpp"));
	}
	else
		UE_LOG(EnemyAI, Error, TEXT("BlackboardComp not found -AIPatrolController.cpp"));

}

void AAIPatrolController::Possess(APawn * Pawn)
{
	Super::Possess(Pawn);

	//Get reference to character
	AICharacter = Cast<AAIPatrol>(Pawn);

	if (AICharacter) //null check
	{
		//Initialize Starting position references
		StartingLocation = AICharacter->GetActorLocation();
		StartingRotation = AICharacter->GetActorRotation();

		if (AICharacter->BehaviorTree->BlackboardAsset) //null check
		{
			if (BlackboardComp)
			{
				//Initialize Blackboard
				BlackboardComp->InitializeBlackboard(*(AICharacter->BehaviorTree->BlackboardAsset));

				//Set IsPatrolKey to AICharacter->bIsPatrol
				BlackboardComp->SetValueAsBool(IsPatrolKey, AICharacter->bIsPatrol);

				//Start BehaviorTree
				BehaviorComp->StartTree(*AICharacter->BehaviorTree);

				//Populate PatrolPoints array
				UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAIPatrolPoint::StaticClass(), PatrolPoints);
			}
			else
				UE_LOG(EnemyAI, Error, TEXT("BlackboardComp not found -AIPatrolController.cpp"));
		}
		else
			UE_LOG(EnemyAI, Error, TEXT("AICharacter->BehaviorTree->BlackboardAsset not found -AIPatrolController.cpp"));
	}
	else
		UE_LOG(EnemyAI, Error, TEXT("AICharacter not found -AIPatrolController.cpp"));
}
