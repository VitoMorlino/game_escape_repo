// Designed and Created by: Vito Morlino

#include "EscapeSaveGame.h"
#include "GAM312_Escape.h"

UEscapeSaveGame::UEscapeSaveGame()
{
	PlayerName = TEXT("EscapePlayer");
	SaveSlotName = TEXT("EscapeSaveSlot");
	UserIndex = 0;
	BestTime = 1337.f;
	BestTimeAllItems = 1337.f;
}


