// Designed and Created by: Vito Morlino

#include "EscapePlayerController.h"

void AEscapePlayerController::PauseGame()
{
	if (GameMode)
	{
		//If Game IsPaused
		if (GameMode->IsPaused())
		{
			if (UnpauseSound)
				if (GetCharacter())
					UGameplayStatics::PlaySound2D(this, UnpauseSound);

			//Unpause the Game
			DisplayWidget(EscapeHUD);
		}
		else
		{
			if (PauseSound)
				if(GetCharacter())
					UGameplayStatics::PlaySound2D(this, PauseSound);

			//Display PauseMenu
			DisplayWidget(PauseMenu);
		}
	}
}

void AEscapePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	check(InputComponent)

	//Actions
	InputComponent->BindAction("Action", IE_Pressed, this, &AEscapePlayerController::BeginPickup);
	InputComponent->BindAction("Action", IE_Released, this, &AEscapePlayerController::EndPickup);
	InputComponent->BindAction("PauseGame", IE_Pressed, this, &AEscapePlayerController::PauseGame).bExecuteWhenPaused = true;
}

void AEscapePlayerController::SetAudioListenerOverride(USceneComponent * AttachToComponent, FVector Location, FRotator Rotation)
{
	Super::SetAudioListenerOverride(AttachToComponent, Location, Rotation);
}

void AEscapePlayerController::BeginPlay()
{
	Super::BeginPlay();

	//Log BeginPlay()
	UE_LOG(PlayerCon, Log, TEXT("BeginPlay called -EscapePlayerController.cpp"));
	
	//Get the GameMode
	GameMode = Cast<AGAM312_EscapeGameModeBase>(GetWorld()->GetAuthGameMode());

	//If GameMode was found
	if (GameMode)
	{
		UE_LOG(EscapeGameMode, Log, TEXT("EscapeGameMode Loaded Successfully -EscapePlayerController.cpp"));

		//Set bIsFirstTimer based on GameMode->IsFirstTimePlaying
		bIsFirstTimer = GameMode->GetIsFirstTimePlaying();
		UE_LOG(PlayerCon, Log, TEXT("bIsFirstTimer = %s -EscapePlayerController.cpp"), bIsFirstTimer ? TEXT("true") : TEXT("false"));

		//If there's a SaveGame loaded
		if (GameMode->LoadedInstance)
		{
			//Set BestTime and BestTimeAllItems to those loaded from SaveGame
			BestTime = GameMode->LoadedInstance->BestTime;
			BestTimeAllItems = GameMode->LoadedInstance->BestTimeAllItems;

			HasSeenHelp = GameMode->LoadedInstance->SavedHelpStruct;
		}
	}
	else
		UE_LOG(EscapeGameMode, Error, TEXT("EscapeGameMode Failed to Load -EscapePlayerController.cpp"));

	/*************************************************************************************************/
	/***** CREATE AND STORE WIDGETS ******************************************************************/
	/*************************************************************************************************/
	/*                                                                                               */
	/***** MainMenu **********************************************************************************/
	if (wMainMenu) //If the widget was set in the Blueprint
		MainMenu = CreateWidget<UUserWidget>(this, wMainMenu);
	else
		UE_LOG(EscapeUI, Error, TEXT("wMainMenu not found -EscapePlayerController.cpp"));
	/*                                                                                               */
	/***** EscapeHUD *********************************************************************************/
	if (wEscapeHUD) //If the widget was set in the Blueprint
		EscapeHUD = CreateWidget<UUserWidget>(this, wEscapeHUD);
	else
		UE_LOG(EscapeUI, Error, TEXT("wEscapeHUD not found -EscapePlayerController.cpp"));
	/*                                                                                               */
	/***** PauseMenu *********************************************************************************/
	if (wPauseMenu) //If the widget was set in the Blueprint
		PauseMenu = CreateWidget<UUserWidget>(this, wPauseMenu);
	else
		UE_LOG(EscapeUI, Error, TEXT("wPauseMenu not found -EscapePlayerController.cpp"));
	/*                                                                                               */
	/***** HelpScreen ********************************************************************************/
	if (wHelpScreen) //If the widget was set in the Blueprint
		HelpScreen = CreateWidget<UUserWidget>(this, wHelpScreen);
	else
		UE_LOG(EscapeUI, Error, TEXT("wHelpScreen not found -EscapePlayerController.cpp"));
	/*                                                                                               */
	/*************************************************************************************************/
	/***** bIsFirstTimer Tutorial Popup Widgets ******************************************************/
	/*************************************************************************************************/
	/*                                                                                               */
	/***** HelpItems *********************************************************************************/
	if (wHelpItems) //If the widget was set in the Blueprint
		HelpItems = CreateWidget<UUserWidget>(this, wHelpItems);
	else
		UE_LOG(EscapeUI, Error, TEXT("wHelpItems not found -EscapePlayerController.cpp"));
	/*                                                                                               */
	/***** HelpLightningSpeed ************************************************************************/
	if (wHelpLightningSpeed) //If the widget was set in the Blueprint
		HelpLightningSpeed = CreateWidget<UUserWidget>(this, wHelpLightningSpeed);
	else
		UE_LOG(EscapeUI, Error, TEXT("wHelpLightningSpeed not found -EscapePlayerController.cpp"));
	/*                                                                                               */
	/***** HelpGhostWalk *****************************************************************************/
	if (wHelpGhostWalk) //If the widget was set in the Blueprint
		HelpGhostWalk = CreateWidget<UUserWidget>(this, wHelpGhostWalk);
	else
		UE_LOG(EscapeUI, Error, TEXT("wHelpGhostWalk not found -EscapePlayerController.cpp"));
	/*                                                                                               */
	/***** HelpGuards ********************************************************************************/
	if (wHelpGuards) //If the widget was set in the Blueprint
		HelpGuards = CreateWidget<UUserWidget>(this, wHelpGuards);
	else
		UE_LOG(EscapeUI, Error, TEXT("wHelpGuards not found -EscapePlayerController.cpp"));
	/*                                                                                               */
}

void AEscapePlayerController::Possess(APawn * aPawn)
{
	Super::Possess(aPawn);

	//Increment NumOfAttempts
	++NumOfAttempts;

	//Log NumOfAttempts
	UE_LOG(PlayerCon, Log, TEXT("Possess called --- NumOfAttempts: %d -EscapePlayerController.cpp"), NumOfAttempts);

	if (GetCharacter())
		//Set Audio Listener to Player Character
		SetAudioListenerOverride(GetCharacter()->GetRootComponent(), FVector::ZeroVector, FRotator::ZeroRotator);
}

void AEscapePlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	//Log that EndPlay was called
	UE_LOG(PlayerCon, Log, TEXT("EndPlay called -EscapePlayerController.cpp"));
}

void AEscapePlayerController::DisplayWidget(UUserWidget * Widget)
{
	//Remove CurrentWidget
	if (CurrentWidget)
		CurrentWidget->RemoveFromParent();

	//Set CurrentWidget to Widget
	if (Widget)
		CurrentWidget = Widget;
	else
		UE_LOG(EscapeUI, Error, TEXT("Widget not found -EscapePlayerController.cpp"));

	//If Widget isn't already in the viewport
	if(!CurrentWidget->IsInViewport())
		//Add Widget to the viewport
		CurrentWidget->AddToViewport();

	//If CurrentWidget IS NOT EscapeHUD
	if (CurrentWidget != EscapeHUD)
	{
		//Unpause Menu Music
		if (CurrentWidget != MainMenu && GameMode->MenuMusicAudioComp)
			GameMode->MenuMusicAudioComp->SetPaused(false);

		//Set game to Paused
		UGameplayStatics::SetGamePaused(GetWorld(), true);
		
		//Show Mouse Cursor
		bShowMouseCursor = true;
		
		//Set Input Mode to UI only
		SetInputMode(FInputModeGameAndUI());
		
		//Set Focus to the CurrentWidget
		CurrentWidget->bIsFocusable = true;
		CurrentWidget->SetUserFocus(this);
	}
	//If CurrentWidget IS EscapeHUD
	else
	{
		//Pause Menu Music
		if (CurrentWidget != MainMenu && GameMode->MenuMusicAudioComp)
			GameMode->MenuMusicAudioComp->SetPaused(true);

		//Set game to NOT Paused
		UGameplayStatics::SetGamePaused(GetWorld(), false);
	
		//Hide Mouse Cursor
		bShowMouseCursor = false;
	
		//Set Input Mode to Game only
		SetInputMode(FInputModeGameOnly());
	}
}

//Try to pick up item
void AEscapePlayerController::BeginPickup()
{
	//Log pickup
	UE_LOG(PlayerCon, Log, TEXT("Begin pickup -EscapePlayerController.cpp"));

	if (GetCharacter())
		//Set bIsPickingUp to true
		Cast<APlayerCharacter>(GetCharacter())->bIsPickingUp = true;
}

//After picking up item
void AEscapePlayerController::EndPickup()
{
	//Log pickup
	UE_LOG(PlayerCon, Log, TEXT("End pickup -EscapePlayerController.cpp"));

	if (GetCharacter())
		//Set bIsPickingUp to false
		Cast<APlayerCharacter>(GetCharacter())->bIsPickingUp = false;
}

void AEscapePlayerController::IncrementCollectedNumOfItems()
{
	//Increment CollectedNumOfItems
	CollectedNumOfItems++;

	//Log collection
	UE_LOG(PlayerCon, Log, TEXT("Player collected an item -EscapePlayerController.cpp"));
}

void AEscapePlayerController::SetIsFirstTimer(bool IsFirstTime)
{
	bIsFirstTimer = IsFirstTime;
	UE_LOG(PlayerCon, Log, TEXT("bIsFirstTimer = %s -EscapePlayerController.cpp"), bIsFirstTimer ? TEXT("true") : TEXT("false"));
}

void AEscapePlayerController::SetBestTime(float NewBest)
{
	BestTime = NewBest;
}

void AEscapePlayerController::SetBestTimeAllItems(float NewBest) 
{ 
	BestTimeAllItems = NewBest; 
}

void AEscapePlayerController::SaveGame()
{
	if (GameMode)
		GameMode->SaveGame(BestTime, BestTimeAllItems, HasSeenHelp);
}