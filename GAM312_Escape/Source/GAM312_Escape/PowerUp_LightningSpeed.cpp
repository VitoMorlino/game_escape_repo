// Designed and Created by: Vito Morlino

#include "PowerUp_LightningSpeed.h"

//Constructor
APowerUp_LightningSpeed::APowerUp_LightningSpeed()
{
	//Set this PowerUpName
	PowerUpName = FString(TEXT("Lightning Speed"));

	//Set this PowerUpDuration
	PowerUpDuration = 10.f;

	//Set PowerUpParticles to particle asset
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAsset(TEXT("/Game/Electrimagic/Particles/P_ElectricOrb.P_ElectricOrb"));
	if (ParticleAsset.Succeeded())
	{
		PowerUpParticles->SetTemplate(ParticleAsset.Object);
	}
}

void APowerUp_LightningSpeed::ActivatePowerUp()
{
	Super::ActivatePowerUp();

	UE_LOG(PowerUp, Warning, TEXT("LightningSpeed Activated -PowerUp_LightningSpeed.cpp"));

	if (MyPlayer)
	{
		//Increase MyPlayer's MaxWalkSpeed
		MyPlayer->IncreaseSpeedBy(SpeedIncrease);

		//Activate Particle Effect on MyPlayer
		MyPlayer->LightningParticles->ActivateSystem();
	}
}

void APowerUp_LightningSpeed::DeactivatePowerUp()
{
	Super::DeactivatePowerUp();

	UE_LOG(PowerUp, Warning, TEXT("LightningSpeed Deactivated -PowerUp_LightningSpeed.cpp"));

	if (MyPlayer)
	{
		//Decrease MyPlayer's MaxWalkSpeed
		MyPlayer->IncreaseSpeedBy(-SpeedIncrease);

		//Deactivate Particle Effect on MyPlayer
		MyPlayer->LightningParticles->DeactivateSystem();
	}
}
