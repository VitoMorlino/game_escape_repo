// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GAM312_Escape.h"
#include "GAM312_EscapeGameModeBase.h"
#include "CameraDirector.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class GAM312_ESCAPE_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

	AGAM312_EscapeGameModeBase* Game;

	//Camera boom positioning the camera behind the character (Note: Camera Boom = SpringArm Component)
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	//Follow camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

public:
	// Sets default values for this character's properties
	APlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	const float WalkSpeed = 300.f; //Default walk speed
	const float SprintModifier = WalkSpeed * 0.5f; //SprintModifier is 0.5x walk speed
	const float BaseTurnRate = 45.0f; //How fast to turn camera with arrow keys
	const float BaseLookUpRate = 45.0f; //How fast to look up/down with arrow keys
	const float InitCameraZoom = 500.0f; //Initial Camera Zoom value
	const float MaxCameraZoom = 200.0f; //Distance from character when zoomed all the way in
	const float MinCameraZoom = 750.0f; //Distance from character when zoomed all the way out
	const float CameraZoomTick = 50.0f; //How much the camera zooms in/out each time scroll wheel up/down
	const float InitJumpVelocity = 425.0f; //Initial jump velocity
	const float InitAirControl = 2.0f; //Initial amount of control player has mid-air
	FRotator InitRotationRate = FRotator(0.0f, 540.0f, 0.0f); //Initial player rotation rate
	bool IsSprinting;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

protected:
	float DefaultArmLength; //Stores initial CameraBoom->TargetArmLength
	float SprintArmLength; //Stores CameraBoom->TargetArmLength while sprinting
	float SprintZoomFactor; //Used to interpolate during sprint to blend camera
	float CameraZoom; //Stores current CameraZoom level

	void UpdateCameraArmLengths();

	//Input functions
	void MoveForward(float Value);
	void MoveRight(float Value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);
	void CameraZoomIn();
	void CameraZoomOut();

	//Jump functionality
	UFUNCTION()
	void OnStartJump(); //sets jump flag when key is pressed
	UFUNCTION()
	void OnStopJump(); //clears jump flag when key is released

	//Sprint functionality
	void OnStartSprint(); //Speed += SprintModifier
	void OnStopSprint(); //Speed -= SprintModifier

	//Camera Change functionality
	template <int Index>
	void ChangeView() { SetView(Index); } //SetView to key pressed
	void SetView(int CamNumber); //Uses CamNumber to call respective function in CameraDirector
	void TogglePerspective();

public:
	UMaterialInterface* GhostMaterial; //Stores reference to GhostMaterial
	UMaterialInterface* PoweredUpMaterial; // Stores reference to PoweredUpMaterial
	UMaterialInterface* UnpoweredMaterial; // Stores reference to UnpoweredMaterial material

	UPROPERTY(EditAnywhere)
	bool bInFirstPerson = false; // True while player is in first person camera mode

	UPROPERTY(EditAnywhere)
	bool bIsPickingUp = false; // True while player is pressing the Action key

	/** Increases or Decreases Player's MaxWalkSpeed
	*	@param Modifier How much to increase or decrease MaxWalkSpeed (Can be + or -)
	*/
	UFUNCTION(BlueprintCallable)
	void IncreaseSpeedBy(float Modifier);
	
	bool bIsPoweredUp = false;
	bool bIsGhost = false;

	//Particle Systems for PowerUps
	UParticleSystemComponent* LightningParticles;
	UParticleSystemComponent* PowerUp2Particles;
	UParticleSystemComponent* PowerUp3Particles;

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};
