// Designed and Created by: Vito Morlino

#include "BTReturnToPost.h"


EBTNodeResult::Type UBTReturnToPost::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	//Get the owning AI controller
	AAIPatrolController* AICon = Cast<AAIPatrolController>(OwnerComp.GetAIOwner());

	if (AICon) //null check
	{
		//Get blackboard comp
		UBlackboardComponent* BlackboardComp = AICon->GetBlackboardComp();

		//Get AIChar
		AAIPatrol* AIChar = AICon->GetAICharacter();
		
		if (AIChar)
		{
			UE_LOG(EnemyAI, Warning, TEXT("Distance from StartingLocation = %f -BTReturnToPost.cpp"), (AIChar->GetActorLocation() - AICon->GetStartingLocation()).Size());

			//If Current Location is 'close enough' to Starting Location
			if ((AIChar->GetActorLocation() - AICon->GetStartingLocation()).Size() <= 0.15f)
			{
				UE_LOG(EnemyAI, Warning, TEXT("Starting Location Check PASSED -BTReturnToPost.cpp"));

				//If Current Rotation is 'close enough' to Starting Rotation
				if ((AIChar->GetActorRotation() - AICon->GetStartingRotation()).IsNearlyZero(0.01f))
				{
					UE_LOG(EnemyAI, Warning, TEXT("Starting Rotation Check PASSED -BTReturnToPost.cpp"));

					//Set ShouldReturnToPost back to false
					if (BlackboardComp)
						BlackboardComp->SetValueAsBool("ShouldReturnToPost", false);

					//Return Success
					return EBTNodeResult::Succeeded;
				}
				else
				{
					UE_LOG(EnemyAI, Error, TEXT("Starting Rotation Check FAILED -BTReturnToPost.cpp"));

					//SetActorRotation to AICon->GetStartingRotation()
					//AICon->SetControlRotation(AICon->GetStartingRotation()); /* Use this to rotate the controller if desired */
					AIChar->SetActorRotation(AICon->GetStartingRotation());
				}

			}
			else
			{
				UE_LOG(EnemyAI, Error, TEXT("Starting Location Check FAILED -BTReturnToPost.cpp"));

				//Move to AICon->GetStartingLocation()
				AICon->MoveToLocation(AICon->GetStartingLocation(), 0.f, false, true, true, false, 0, true);
				UE_LOG(EnemyAI, Warning, TEXT("Moving to Starting Location -BTReturnToPost.cpp"));
			}
		}
	}

	//Return Failure
	return EBTNodeResult::Failed;
}


