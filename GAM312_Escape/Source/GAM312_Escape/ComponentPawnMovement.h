// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "ComponentPawnMovement.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API UComponentPawnMovement : public UPawnMovementComponent
{
	GENERATED_BODY()
	
public:
	//Override TickComponent()
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	
	
};
