// Designed and Created by: Vito Morlino

#include "BTSelectPatrolPoint.h"


EBTNodeResult::Type UBTSelectPatrolPoint::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	//Get the owning AI controller
	AAIPatrolController* AICon = Cast<AAIPatrolController>(OwnerComp.GetAIOwner());

	if (AICon) //null check
	{
		//Get blackboard comp
		UBlackboardComponent* BlackboardComp = AICon->GetBlackboardComp();

		//Get current patrol point from AICon's LocationToGo blackboard key
		AAIPatrolPoint* CurrentPoint = Cast<AAIPatrolPoint>(BlackboardComp->GetValueAsObject("LocationToGo"));

		//Get the array of available patrol points from AICon
		TArray<AActor*> AvailablePatrolPoints = AICon->GetPatrolPoints();

		//Declare a NextPatrolPoint to store next LocationToGo
		AAIPatrolPoint* NextPatrolPoint = nullptr;

		//If we haven't reached the end of the array (last patrol point)
		if (AICon->CurrentPatrolPoint != AvailablePatrolPoints.Num() - 1)
		{
			//Set NextPatrolPoint to CurrentPatrolPoint + 1
			NextPatrolPoint = Cast<AAIPatrolPoint>(AvailablePatrolPoints[++AICon->CurrentPatrolPoint]);
			UE_LOG(EnemyAI, Log, TEXT("Set Next Patrol Point"));
		}
		else // If there are no more points to go to
		{
			//Set NextPatrolPoint to the first PatrolPoint
			NextPatrolPoint = Cast<AAIPatrolPoint>(AvailablePatrolPoints[0]);
			AICon->CurrentPatrolPoint = 0;
			UE_LOG(EnemyAI, Log, TEXT("Set First Patrol Point"));
		}

		//Set the LocationToGo blackboard key to NextPatrolPoint
		BlackboardComp->SetValueAsObject("LocationToGo", NextPatrolPoint);

		return EBTNodeResult::Succeeded;
	}
	return EBTNodeResult::Failed;
}

