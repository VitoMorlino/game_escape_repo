// Designed and Created by: Vito Morlino

#include "Spawner_PickupItem.h"


// Sets default values
ASpawner_PickupItem::ASpawner_PickupItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawner_PickupItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawner_PickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

