// Designed and Created by: Vito Morlino

#include "BTPokePlayer.h"


EBTNodeResult::Type UBTPokePlayer::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	//Get the owning AI controller
	AAIPatrolController* AICon = Cast<AAIPatrolController>(OwnerComp.GetAIOwner());

	if (AICon) //null check
	{
		//Get AI Character
		AAIPatrol* AIChar = Cast<AAIPatrol>(AICon->GetCharacter());

		if (AIChar) //null check
		{ 
			//Get blackboard comp
			UBlackboardComponent* BlackboardComp = AICon->GetBlackboardComp();

			if (BlackboardComp)
			{
				//Get Player Character
				APlayerCharacter* PlayerChar = Cast<APlayerCharacter>(BlackboardComp->GetValueAsObject("Target"));

				if (PlayerChar) //null check
				{
					if (AIChar->GetDistanceTo(PlayerChar) <= 80.f)
					{
						//Destroy Player Character
						PlayerChar->Destroy();

						//Get an array of all AIControllers
						TArray<AActor*> AllAIControllers;
						UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAIPatrolController::StaticClass(), AllAIControllers);

						//Call LostPlayer() on each of AllAIControllers
						for (AActor* Actor : AllAIControllers)
						{
							AAIPatrolController* ThisAICon = Cast<AAIPatrolController>(Actor);

							if (ThisAICon)
								ThisAICon->LostPlayer();
							else
								UE_LOG(EnemyAI, Error, TEXT("ThisAICon not found -BTPokePlayer.cpp"));

						}

						UE_LOG(EnemyAI, Warning, TEXT("Player Destroyed -BTPokePlayer.cpp"));
						UE_LOG(EnemyAI, Error, TEXT("AIChar->GetDistanceTo(PlayerChar) = %f -BTPokePlayer.cpp"), AIChar->GetDistanceTo(PlayerChar));
					}
					else
					{
						UE_LOG(EnemyAI, Error, TEXT("Player Out of Range -BTPokePlayer.cpp"), AIChar->GetDistanceTo(PlayerChar));
						UE_LOG(EnemyAI, Error, TEXT("AIChar->GetDistanceTo(PlayerChar) = %f -BTPokePlayer.cpp"), AIChar->GetDistanceTo(PlayerChar));
					}

					

					//Return Success
					return EBTNodeResult::Succeeded;
				}
				else
					UE_LOG(EnemyAI, Error, TEXT("PlayerChar not found -BTPokePlayer.cpp"));
			}
			else
				UE_LOG(EnemyAI, Error, TEXT("BlackboardComp not found -BTPokePlayer.cpp"));
		}
		else
			UE_LOG(EnemyAI, Error, TEXT("AIChar not found -BTPokePlayer.cpp"));
	}
	else
		UE_LOG(EnemyAI, Error, TEXT("AICon not found -BTPokePlayer.cpp"));

	//Return Failure
	return EBTNodeResult::Failed;
}

