// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "PowerUp.h"
#include "PowerUp_GhostWalk.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API APowerUp_GhostWalk : public APowerUp
{
	GENERATED_BODY()
	
public:
	APowerUp_GhostWalk();

private:
	void ActivatePowerUp() override;
	void DeactivatePowerUp() override;
	float SpeedDecrease = 200.f;
	
};
