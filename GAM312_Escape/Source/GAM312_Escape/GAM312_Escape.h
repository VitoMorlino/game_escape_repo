// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "EscapeSaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

DECLARE_LOG_CATEGORY_EXTERN(EnemyAI, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(PlayerCon, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(PlayerChar, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(PowerUp, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(EscapeUI, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(EscapeGameMode, Log, All);