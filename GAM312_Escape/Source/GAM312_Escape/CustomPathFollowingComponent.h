// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GAM312_Escape.h"
#include "Navigation/PathFollowingComponent.h"
#include "CustomPathFollowingComponent.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API UCustomPathFollowingComponent : public UPathFollowingComponent
{
	GENERATED_BODY()
	
public:
	// Constructor
	UCustomPathFollowingComponent(const FObjectInitializer& ObjectInitializer);

	/** follow current path segment */
	virtual void FollowPathSegment(float DeltaTime) override;

	/** sets variables related to current move segment */
	virtual void SetMoveSegment(int32 SegmentStartIndex) override;

	/** check state of path following, update move segment if needed */
	virtual void UpdatePathSegment() override;
	
	
};
