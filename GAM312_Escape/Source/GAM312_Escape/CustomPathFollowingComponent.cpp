// Designed and Created by: Vito Morlino

#include "CustomPathFollowingComponent.h"
#include "AIPatrol.h"
#include "AIPatrolController.h"


//Constructor
UCustomPathFollowingComponent::UCustomPathFollowingComponent(const FObjectInitializer & ObjectInitializer)
{
}

void UCustomPathFollowingComponent::FollowPathSegment(float DeltaTime)
{
	//Pointer Safety Checks
	if (MovementComp == NULL || !Path.IsValid())
	{
		return;
	}

	//If partial path is detected
	if (Path->IsPartial()) //AI could not reach player
	{
		//I send out instructions to my custom character class here - Rama
		//JoyChar->ReceiveJumpFallPathingRequest(); - Rama

		/** SKIP TRYING TO JUMP, AND INSTEAD JUST - Vito
		*	LOG THAT WE GOT A PARTIAL PATH */
		UE_LOG(EnemyAI, Warning, TEXT("Partial Path Detected -CustomPathFollowingComponent.cpp"));
		
		AAIPatrolController* AICon = Cast<AAIPatrolController>(GetOwner());

		if (AICon)
		{
			AICon->LostPlayer();
			UE_LOG(EnemyAI, Warning, TEXT("LostPlayer() called -CustomPathFollowingComponent.cpp"));
		}
		else
			UE_LOG(EnemyAI, Error, TEXT("AICon not found -CustomPathFollowingComponent.cpp"));
		return;
		//~~~
	}


	//Call Super FollowPathSegment (Proceed as normal)
	Super::FollowPathSegment(DeltaTime);
}

void UCustomPathFollowingComponent::SetMoveSegment(int32 SegmentStartIndex)
{
	//Call Super SetMoveSegment
	Super::SetMoveSegment(SegmentStartIndex);
}

void UCustomPathFollowingComponent::UpdatePathSegment()
{
	//Call Super UpdatePathSegment
	Super::UpdatePathSegment();
}

