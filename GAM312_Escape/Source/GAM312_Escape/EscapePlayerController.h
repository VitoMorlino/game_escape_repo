// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GAM312_Escape.h"
#include "PlayerCharacter.h"
#include "UserWidget.h"
#include "GAM312_EscapeGameModeBase.h"
#include "EscapeSaveGame.h"
#include "GameFramework/PlayerController.h"
#include "EscapePlayerController.generated.h"



/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API AEscapePlayerController : public APlayerController
{
	GENERATED_BODY()
	
	virtual void BeginPlay() override;

	virtual void Possess(APawn* aPawn) override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	AGAM312_EscapeGameModeBase* GameMode;

	//True if no SaveGame was found by GameMode
	bool bIsFirstTimer;

	//Best time completing the level (regardless of items collected)
	float BestTime;
	//Best time completing the level after collecting ALL items
	float BestTimeAllItems;

	//The number of tries player took to complete the level
	int32 NumOfAttempts = 0;

	//The number of items this player has collected so far
	int32 CollectedNumOfItems = 0;
	
	//Pause the game and display PauseMenu
	void PauseGame();

	//Sets PlayerCharacter bIsPickingUp = true
	void BeginPickup();
	//Sets PlayerCharacter bIsPickingUp = false
	void EndPickup();

public:
	// Called to bind functionality to input
	void SetupInputComponent() override;

	UFUNCTION(BlueprintCallable, Category = "Game|Audio")
	void SetAudioListenerOverride(USceneComponent* AttachToComponent, FVector Location, FRotator Rotation);

	// Reference to Widget in the Editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> wEscapeHUD;
	// Reference to Widget in the Editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> wMainMenu;
	// Reference to Widget in the Editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> wPauseMenu;
	// Reference to Widget in the Editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> wDeathMenu;
	// Reference to Widget in the Editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> wHelpScreen;

	// Reference to Tutorial Popup Widget in the Editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TutorialWidgets")
	TSubclassOf<class UUserWidget> wHelpItems;
	// Reference to Tutorial Popup Widget in the Editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TutorialWidgets")
	TSubclassOf<class UUserWidget> wHelpLightningSpeed;
	// Reference to Tutorial Popup Widget in the Editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TutorialWidgets")
	TSubclassOf<class UUserWidget> wHelpGhostWalk;
	// Reference to Tutorial Popup Widget in the Editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TutorialWidgets")
	TSubclassOf<class UUserWidget> wHelpGuards;

	// Widget Pointer
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* CurrentWidget;
	// Widget Pointer
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* EscapeHUD;
	// Widget Pointer
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* MainMenu;
	// Widget Pointer
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* PauseMenu;
	// Widget Pointer
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* HelpScreen;

	// Tutorial Popup Widget Pointer
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* HelpItems;
	// Tutorial Popup Widget Pointer
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* HelpLightningSpeed;
	// Tutorial Popup Widget Pointer
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* HelpGhostWalk;
	// Tutorial Popup Widget Pointer
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* HelpGuards;

	/** Holds whether or not the player has seen each Tutorial Popup (all initialized to false)
	***
	*	DisableTutorialPopups = false;
	*	HasSeenItems = false;
	*	HasSeenLightningSpeed = false;
	*	HasSeenGhostWalk = false;
	*	HasSeenGuards = false; 
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HelpStruct")
	FStructHasSeenHelp HasSeenHelp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundCue* PauseSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundCue* UnpauseSound;

	/**	Add Widget to viewport and update inputmode/visibility as necessary
	*	@param Widget The widget to be displayed */
	UFUNCTION(BlueprintCallable)
	void DisplayWidget(UUserWidget* Widget);

	/**	Returns PlayerController's NumOfAttempts */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE int32 GetNumOfAttempts() { return NumOfAttempts; }

	/**	Returns PlayerController's CollectedNumOfItems */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE int32 GetCollectedNumOfItems() { return CollectedNumOfItems; }

	/* Increment CollectedNumOfItems */
	UFUNCTION(BlueprintCallable)
	void IncrementCollectedNumOfItems();

	/**	Returns PlayerController's bIsFirstTimer */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool GetIsFirstTimer() { return bIsFirstTimer; }

	/**	Returns PlayerController's BestTime */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE float GetBestTime() { return BestTime; }

	/**	Returns PlayerController's BestTimeAllItems */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE float GetBestTimeAllItems() { return BestTimeAllItems; }

	/**	Set PlayerController's bIsFirstTimer
	*	@param IsFirstTime Value to set */
	UFUNCTION(BlueprintCallable)
	void SetIsFirstTimer(bool IsFirstTime);

	/**	Set PlayerController's BestTime
	*	@param NewBest New BestTime to set */
	UFUNCTION(BlueprintCallable)
	void SetBestTime(float NewBest);

	/**	Set PlayerController's BestTimeAllItems
	*	@param NewBest New BestTime to set */
	UFUNCTION(BlueprintCallable)
	void SetBestTimeAllItems(float NewBest);

	/**	Saves this PlayerController's Best Times */
	UFUNCTION(BlueprintCallable)
	void SaveGame();

};
