// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "PowerUp.h"
#include "PowerUp_LightningSpeed.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API APowerUp_LightningSpeed : public APowerUp
{
	GENERATED_BODY()


public:
	APowerUp_LightningSpeed();

private:
	void ActivatePowerUp() override;
	void DeactivatePowerUp() override;
	float SpeedIncrease = 200.f;
	
};
