// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "AIPatrolController.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTReturnToPost.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API UBTReturnToPost : public UBTTaskNode
{
	GENERATED_BODY()
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
	
};
