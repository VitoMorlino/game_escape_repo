// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "EscapeSaveGame.generated.h"

USTRUCT()
struct FStructHasSeenHelp
{
	GENERATED_BODY()

	//Declare Variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Help Struct")
	bool DisableTutorialPopups;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Help Struct")
	bool HasSeenItems;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Help Struct")
	bool HasSeenLightningSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Help Struct")
	bool HasSeenGhostWalk;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Help Struct")
	bool HasSeenGuards;

	//Set Functions
	void SetDisableTutorialPopups(bool NewValue)
	{
		DisableTutorialPopups = NewValue;
	}
	void SetHasSeenItems(bool NewValue)
	{
		HasSeenItems = NewValue;
	}
	void SetHasSeenLightningSpeed(bool NewValue)
	{
		HasSeenLightningSpeed = NewValue;
	}
	void SetHasSeenGhostWalk(bool NewValue)
	{
		HasSeenGhostWalk = NewValue;
	}
	void SetHasSeenGuards(bool NewValue)
	{
		HasSeenGuards = NewValue;
	}

	//Get Functions
	bool GetDisableTutorialPopups()
	{
		return DisableTutorialPopups;
	}
	bool GetHasSeenItems()
	{
		return HasSeenItems;
	}
	bool GetHasSeenLightningSpeed()
	{
		return HasSeenLightningSpeed;
	}
	bool GetHasSeenGhostWalk()
	{
		return HasSeenGhostWalk;
	}
	bool GetHasSeenGuards()
	{
		return HasSeenGuards;
	}

	//Constructor
	FStructHasSeenHelp()
	{
		//Initialize Variables
		DisableTutorialPopups = false;
		HasSeenItems = false;
		HasSeenLightningSpeed = false;
		HasSeenGhostWalk = false;
		HasSeenGuards = false;
	}
};

/**
 * 
 */
UCLASS()
class GAM312_ESCAPE_API UEscapeSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	UEscapeSaveGame();
	
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FString PlayerName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	FString SaveSlotName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	uint32 UserIndex;

	UPROPERTY(VisibleAnywhere, Category = Escape)
	FStructHasSeenHelp SavedHelpStruct;

	UPROPERTY(VisibleAnywhere, Category = Escape)
	float BestTime;

	UPROPERTY(VisibleAnywhere, Category = Escape)
	float BestTimeAllItems;

};
