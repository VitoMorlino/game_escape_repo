// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "ComponentPawnMovement.h"
#include "ComponentPawn.generated.h"

UCLASS()
class GAM312_ESCAPE_API AComponentPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AComponentPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Pointers to our components
	UParticleSystemComponent *OurParticleSystem;
	class UComponentPawnMovement* OurMovementComponent;

	// Override GetMovementComponent()
	virtual UPawnMovementComponent* GetMovementComponent() const override;

	// Movement functions
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void Turn(float AxisValue);
};
