// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/PawnSensingComponent.h"
#include "PlayerCharacter.h"
#include "AIPatrol.generated.h"

UCLASS()
class GAM312_ESCAPE_API AAIPatrol : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAIPatrol();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	//Stores this AICharacter's BehaviorTree
	UPROPERTY(EditAnywhere, Category = AI)
	class UBehaviorTree* BehaviorTree;

	//Stores this AICharacter's PawnSensingComponent
	UPROPERTY(VisibleAnywhere, Category = AI)
	class UPawnSensingComponent* PawnSensingComp;

	//If true, this AI will follow PatrolPoints
	//If false, this AI will stand still until player is spotted
	UPROPERTY(EditAnywhere, Category = AI)
	bool bIsPatrol = true;


private:
	//DefaultWalkSpeed of AICharacters
	float DefaultWalkSpeed = 200.f;

	//Called when AICharacter senses player with PawnSensingComponent
	UFUNCTION()
	void OnPlayerCaught(APawn* Pawn);

	//Material References
	UMaterialInterface* MatPulse; // Stores reference to MatPulse material
	UMaterialInterface* MatNoPulse; // Stores referemce to MatNoPulse material
	
public:
	//Inline Gets
	FORCEINLINE UMaterialInterface* GetMatPulse() const { return MatPulse; }
	FORCEINLINE UMaterialInterface* GetMatNoPulse() const { return MatNoPulse; }
	FORCEINLINE float GetDefaultWalkSpeed() const { return DefaultWalkSpeed; }
};
