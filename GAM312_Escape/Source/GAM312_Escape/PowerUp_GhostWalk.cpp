// Designed and Created by: Vito Morlino

#include "PowerUp_GhostWalk.h"


//Constructor
APowerUp_GhostWalk::APowerUp_GhostWalk()
{
	//Set this PowerUpName
	PowerUpName = FString(TEXT("Ghost Walk"));

	//Set this PowerUpDuration
	PowerUpDuration = 5.f;
}

void APowerUp_GhostWalk::ActivatePowerUp()
{
	Super::ActivatePowerUp();

	//Set player character's mesh material to GhostMaterial
	MyPlayer->GetMesh()->SetMaterial(0, MyPlayer->GhostMaterial);
	MyPlayer->GetMesh()->SetMaterial(1, MyPlayer->GhostMaterial);

	//Increase MyPlayer's MaxWalkSpeed
	MyPlayer->IncreaseSpeedBy(-SpeedDecrease);

	MyPlayer->bIsGhost = true;
}

void APowerUp_GhostWalk::DeactivatePowerUp()
{
	Super::DeactivatePowerUp();

	//Decrease MyPlayer's MaxWalkSpeed
	MyPlayer->IncreaseSpeedBy(SpeedDecrease);

	MyPlayer->bIsGhost = false;

	/* Mesh Material is reset to default in Super::DeactivatePowerUp() */
}
