// Designed and Created by: Vito Morlino

#include "ActorSpawner.h"
#include "PowerUp.h"


// Sets default values
AActorSpawner::AActorSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BillboardComp = CreateDefaultSubobject<UBillboardComponent>(TEXT("BillboardComp"));
	RootComponent = BillboardComp;

	// Who did the spawning? This ActorSpawner did.
	SpawnParams.Owner = this;

}

// Called when the game starts or when spawned
void AActorSpawner::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnActor();

}

void AActorSpawner::SpawnActor()
{
	if (ActorToSpawn)
	{
		//Spawn ActorToSpawn at this ActorSpawner's location/rotation
		AActor* NewObject = GetWorld()->SpawnActor<AActor>(ActorToSpawn, GetActorLocation(), GetActorRotation(), SpawnParams);
		
		//If ActorToSpawn is a PowerUp AND PowerUpDurationOverride is > 0
		if (NewObject->IsA(APowerUp::StaticClass())
			&& PowerUpDurationOverride > 0)
		{
			//Set the PowerUp's duration to PowerUpDurationOverride
			Cast<APowerUp>(NewObject)->PowerUpDuration = PowerUpDurationOverride;
		}

		//If spawn was successful
		if (NewObject)
		{
			UE_LOG(LogTemp, Log, TEXT("Actor Spawn SUCCEEDED -ActorSpawner.cpp"));

			//Bind function OnNewObjectDestroyed to NewObject's OnDestroyed function
			NewObject->OnDestroyed.AddDynamic(this, &AActorSpawner::OnNewObjectDestroyed);
		}
		else
			UE_LOG(LogTemp, Error, TEXT("Actor Spawn FAILED -ActorSpawner.cpp"));
	}
	else
		UE_LOG(LogTemp, Error, TEXT("ActorToSpawn not found -ActorSpawner.cpp"));
}

void AActorSpawner::OnNewObjectDestroyed(AActor* Actor)
{
	UE_LOG(LogTemp, Warning, TEXT("OnNewObjectDestroyed Called -ActorSpawner.cpp"));

	if (TimeBetweenSpawns > 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("TimeBetweenSpawns = %f --- Spawning when time expires -ActorSpawner.cpp"), TimeBetweenSpawns);

		//Start timer that calls RespawnActor() on expiration
		GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &AActorSpawner::RespawnActor, TimeBetweenSpawns, false);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("TimeBetweenSpawns <= 0 --- Spawning Immediately -ActorSpawner.cpp"));
		RespawnActor();
	}
}

void AActorSpawner::RespawnActor()
{
	SpawnActor();
	UE_LOG(LogTemp, Warning, TEXT("Respawning ActorToSpawn -ActorSpawner.cpp"));
}
