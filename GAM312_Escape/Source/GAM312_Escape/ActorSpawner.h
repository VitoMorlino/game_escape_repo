// Designed and Created by: Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GAM312_Escape.h"
#include "GameFramework/Actor.h"
#include "ActorSpawner.generated.h"

UCLASS()
class GAM312_ESCAPE_API AActorSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AActorSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Spawn ActorToSpawn
	void SpawnActor();

	// Called when RespawnTimerHandle expires
	void RespawnActor();

	// Called when the spawned actor is destroyed
	UFUNCTION()
	void OnNewObjectDestroyed(AActor* Actor);

public:	
	UBillboardComponent* BillboardComp;

	//The Actor that will be spawned when SpawnActor() is called
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> ActorToSpawn;

	//Hold SpawnParameters
	FActorSpawnParameters SpawnParams;

	//Overrides PowerUpDuration if set > 0 AND ActorToSpawn is a PowerUp
	UPROPERTY(EditAnywhere)
	float PowerUpDurationOverride;

	/** Time (in seconds) after the spawned object
	*	is destroyed until a new one is spawned */
	UPROPERTY(EditAnywhere)
	float TimeBetweenSpawns;

	FTimerHandle RespawnTimerHandle;

};
