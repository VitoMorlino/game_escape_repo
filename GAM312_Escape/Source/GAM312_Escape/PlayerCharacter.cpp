// Designed and Created by: Vito Morlino

#include "PlayerCharacter.h"


// Sets default values
APlayerCharacter::APlayerCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = InitRotationRate; 
	GetCharacterMovement()->JumpZVelocity = InitJumpVelocity;
	GetCharacterMovement()->AirControl = InitAirControl;
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	GetCharacterMovement()->MaxWalkSpeedCrouched = 0.f; // Stop movement while crouched
	GetCharacterMovement()->CrouchedHalfHeight = 80.f; // Resizes character (including camera height) when crouched

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = InitCameraZoom; // Initialize camera zoom	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create reference to pulsing material for character mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> GhostMatAsset(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile_Pulse_Ghost.M_Tech_Hex_Tile_Pulse_Ghost"));
	if (GhostMatAsset.Succeeded())
		GhostMaterial = GhostMatAsset.Object;

	// Create reference to pulsing material for character mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MatPulseAsset(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile_Pulse.M_Tech_Hex_Tile_Pulse"));
	if (MatPulseAsset.Succeeded())
		PoweredUpMaterial = MatPulseAsset.Object;

	// Create reference to non-pulsing material for character mesh
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MatNoPulseAsset(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile.M_Tech_Hex_Tile"));
	if (MatNoPulseAsset.Succeeded())
		UnpoweredMaterial = MatNoPulseAsset.Object;

	//Create Particle System for PowerUp_LightningSpeed
	LightningParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("LightningParticles"));
	LightningParticles->SetupAttachment(GetMesh(), "root");
	LightningParticles->bAutoActivate = false;
	static ConstructorHelpers::FObjectFinder<UParticleSystem> LightningParticleAsset(TEXT("/Game/Electrimagic/Particles/P_ElectricOrbMod.P_ElectricOrbMod"));
	if (LightningParticleAsset.Succeeded())
	{
		LightningParticles->SetTemplate(LightningParticleAsset.Object);
	}
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	Game = Cast<AGAM312_EscapeGameModeBase>(GetWorld()->GetAuthGameMode());

	//Initialize Camera Zoom and Default/Sprint Arm Lengths
	UpdateCameraArmLengths();
	CameraZoom = CameraBoom->TargetArmLength;
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/**********************************************************************************************************/
	/****** Zoom Out while sprinting **************************************************************************/
	/*                                                                                                        */
	{
		if (IsSprinting)
		{
			SprintZoomFactor += DeltaTime / 0.25f; //Zoom out over quarter of a second
		}
		else
		{
			SprintZoomFactor -= DeltaTime / 0.5f; //Zoom back in over a half a second
		}
		SprintZoomFactor = FMath::Clamp<float>(SprintZoomFactor, 0.0f, 1.0f);

		//Blend our camera's FOV and our SpringArm's length based on ZoomFactor
		FollowCamera->FieldOfView = FMath::Lerp<float>(90.0f, 100.0f, SprintZoomFactor);
		CameraBoom->TargetArmLength = FMath::Lerp<float>(DefaultArmLength, SprintArmLength, SprintZoomFactor);
	}
	/*                                                                                                         */
	/***********************************************************************************************************/

	/***************************************************************/
	/****** Set to Crouched while bIsPickingUp *********************/
	/*                                                             */
	{
		if (bIsPickingUp)
		{
			if (CanCrouch())
			{
				//Start Crouching
				GetCharacterMovement()->bWantsToCrouch = true;
				//Don't turn character with movement
				GetCharacterMovement()->bOrientRotationToMovement = false;
				//Log crouch
				UE_LOG(PlayerChar, Log, TEXT("Started Crouching"));
			}
		}
		else
			if (bIsCrouched)
			{
				//Stop Crouching
				GetCharacterMovement()->bWantsToCrouch = false;
				//Don't turn character with movement
				GetCharacterMovement()->bOrientRotationToMovement = true;
				//Log crouch
				UE_LOG(PlayerChar, Log, TEXT("Stopped Crouching"));
			}
	}
	/*                                                             */
	/***************************************************************/
}

//Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	check(InputComponent);

	//Move Character
	InputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);

	//Look Around
	InputComponent->BindAxis("Turn", this, &APlayerCharacter::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &APlayerCharacter::AddControllerPitchInput);
	InputComponent->BindAxis("TurnAtRate", this, &APlayerCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUpAtRate", this, &APlayerCharacter::LookUpAtRate);

	//Zoom In/Out
	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &APlayerCharacter::CameraZoomIn);
	InputComponent->BindAction("ZoomOut", IE_Pressed, this, &APlayerCharacter::CameraZoomOut);

	//Jump/Sprint
	InputComponent->BindAction("Jump", IE_Pressed, this, &APlayerCharacter::OnStartJump);
	InputComponent->BindAction("Jump", IE_Released, this, &APlayerCharacter::OnStopJump);
	InputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerCharacter::OnStartSprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &APlayerCharacter::OnStopSprint);

	//Change Camera/view
	InputComponent->BindAction("TogglePerspective", IE_Pressed, this, &APlayerCharacter::TogglePerspective);
	InputComponent->BindAction("OverheadView", IE_Pressed, this, &APlayerCharacter::ChangeView<1>);
	InputComponent->BindAction("OverheadView", IE_Released, this, &APlayerCharacter::ChangeView<2>);

}

//Input functions
void APlayerCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void APlayerCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void APlayerCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::UpdateCameraArmLengths()
{
	DefaultArmLength = CameraBoom->TargetArmLength; //Default Arm Length
	SprintArmLength = DefaultArmLength * 1.1f; //SprintArmLength is 1.1x Default
}

void APlayerCharacter::CameraZoomIn()
{
	//If CameraZoom is not already at MaxCameraZoom
	if (CameraZoom != MaxCameraZoom && !bInFirstPerson)
	{
		//Update CameraZoom
		CameraZoom -= CameraZoomTick;

		if (CameraZoom <= MaxCameraZoom)
		{
			//Set to Max in case we overshot
			CameraBoom->TargetArmLength = MaxCameraZoom;
			CameraZoom = MaxCameraZoom;

			// screen message
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Max Camera Zoom Reached!"));
		}
		else
		{
			//Set ArmLength to CameraZoom
			CameraBoom->TargetArmLength = CameraZoom;

			// screen message
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Zoomed in"));
		}

		UpdateCameraArmLengths(); //Adjust Default and Sprint Arm Lengths
	}
	else if (!bInFirstPerson) //if CameraZoom is at MaxCameraZoom
	{
		TogglePerspective(); //Call TogglePerspective() to switch to first person
	}
}

void APlayerCharacter::CameraZoomOut()
{
	//If CameraZoom is not already at MinCameraZoom
	if (CameraZoom != MinCameraZoom)
	{
		//Update CameraZoom
		if (bInFirstPerson)
		{
			//Set zoom to the MaxCameraZoom and turn off bInFirstPerson
			CameraZoom = MaxCameraZoom;
			TogglePerspective();
		}
		else
			//Zoom out
			CameraZoom += CameraZoomTick;

		if (CameraZoom >= MinCameraZoom)
		{
			//Set to Min in case we overshot
			CameraBoom->TargetArmLength = MinCameraZoom;
			CameraZoom = MinCameraZoom;

			// screen message
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Min Camera Zoom Reached!"));
		}
		else
		{
			//Set ArmLength to CameraZoom
			CameraBoom->TargetArmLength = CameraZoom;

			// screen message
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Zoomed out"));
		}

		UpdateCameraArmLengths(); //Adjust Default and Sprint Arm Lengths
	}
}

//Change Camera Views
void APlayerCharacter::SetView(int index)
{
	//Iterate through actors to find the Camera Director
	for (TActorIterator<ACameraDirector> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		//Call function, based on index (sent from ChangeView()), in CameraDirector (found by iterator) 
		if (index == 1)
		{
			ActorItr->StartOverheadView();
		}
		else if (index == 2)
		{
			ActorItr->StopOverheadView();
		}
		break;
	}
}

void APlayerCharacter::TogglePerspective()
{
	//If Player is in First Person, set to Third person - else, set to First Person
	if (bInFirstPerson)
	{
		//Set to Third Person
		CameraBoom->TargetArmLength = InitCameraZoom;
		CameraBoom->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		UpdateCameraArmLengths();
		bUseControllerRotationYaw = false; //Don't rotate character with camera
		//GetMesh()->SetOwnerNoSee(false); //Show character mesh
		bInFirstPerson = false; //Toggle bInFirstPerson bool

		UE_LOG(PlayerChar, Log, TEXT("Set Perspective to Third Person"));
	}
	else
	{
		//Set to First Person
		CameraBoom->TargetArmLength = 0.0f;
		CameraBoom->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "first_person_head");
		UpdateCameraArmLengths();
		bUseControllerRotationYaw = true; //Rotate character with camera
		//GetMesh()->SetOwnerNoSee(true); //Hide character mesh
		bInFirstPerson = true; //Toggle bInFirstPerson bool

		UE_LOG(PlayerChar, Log, TEXT("Set Perspective to Third Person"));
	}
}

// Utilize jump functionality pre-built into Character.h to
// set the jump flag to true when jump starts
void APlayerCharacter::OnStartJump()
{
	bPressedJump = true;
}

// Utilize jump functionality pre-built into Character.h to
// set the jump flag to false when jump ends
void APlayerCharacter::OnStopJump()
{
	bPressedJump = false;
}

//When Sprint Key is Pressed
void APlayerCharacter::OnStartSprint()
{
	//If IsSprinting = false
	if (!IsSprinting)
	{
		//Set IsSprinting to true
		IsSprinting = true;

		//Add SprintModifier
		GetCharacterMovement()->MaxWalkSpeed += SprintModifier;
	}
}

//When Sprint Key is Released
void APlayerCharacter::OnStopSprint()
{
	//If IsSprinting = true
	if (IsSprinting)
	{
		//Set IsSprinting to false
		IsSprinting = false;

		//Subtract SprintModifier
		GetCharacterMovement()->MaxWalkSpeed -= SprintModifier;
	}
}

//Increase Player's speed by adding Modifier
void APlayerCharacter::IncreaseSpeedBy(float Modifier)
{
	GetCharacterMovement()->MaxWalkSpeed += Modifier;
}
